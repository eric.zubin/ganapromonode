'use strict'

var Parte=require('../models/parte');
var Servicio=require('../models/servicio');
var ParteServcio=require('../models/parteservicio');
var RFID=require('../models/rfid');

//var fs=require('fs');
//var path=require('path');
var mongooePaginate=require('mongoose-pagination');

function prueba (req,res){
    res.status(200).send({
        msesage: 'Hola Mundo desde el controlador parte'
    });
    //correo.enviarCorreo("eric.zubin@gmail.com");
} 
function saveparte (req,res){

    var params=req.body;

    var parte=new Parte();

        parte.component =params.component;
        //parte.LineIn =params.LineIn;

    
        parte.save((err,parteStored)=>{
            
            console.log(err);

            if(err) return  res.status(500).send({msesage: 'Error al guardar la parte'});

            if(parteStored){
                res.status(200).send({Parte: parteStored}); 
            }else{

                res.status(404).send({msesage: 'No se ah registrado el usuario'});
            }
        });

} 


//Funcion del metodo borrar Parte
function deleteParte(req,res){
  
    Parte.remove({ _id: req.body.id }, function(err) {
        if (!err) {
            res.status(200).send({
                msesage: 'Delete Parte'
            });
        }
        else {
            res.status(500).send({
                msesage: 'Delete HerramiParteenta'
            });
        }
    });
    //correo.enviarCorreo("eric.zubin@gmail.com");
} 


//Funcion del metodo borrar Parte
function updateParte (req,res){
    var params=req.body;
  

    Parte.findOneAndUpdate({"_id": params._id}, 
        {$set:{component:params.component  }},
        
        {new: true}, function(err, parte){
       
            if (!err) {
                res.status(200).send({
                    parte
                });
            }
            else {
                res.status(500).send({
                    msesage: 'Delete HerramiParteenta'
                });
            }
    });





 
  
    //correo.enviarCorreo("eric.zubin@gmail.com");
} 

//Funcion del metodo borrar Parte
function getSParte(req,res){
    res.status(200).send({
        msesage: 'Get Parte'
    });
    //correo.enviarCorreo("eric.zubin@gmail.com");
} 

//Funcion del metodo borrar Parte
function getPartes(req,res){
   
 
    Parte.find({}, (err,partes) =>{
        if(err) return  res.status(500).send({msesage: 'Error en la petición'});
        if(!partes) return  res.status(404).send({msesage: 'No existe partes'});
        if(partes) return  res.status(200).send({
            partes,
        
        });
     
    });
    //correo.enviarCorreo("eric.zubin@gmail.com");
} 

//Funcion del metodo borrar Parte
function getPartesServicio(req,res){
    const query = req.query;
  

    ParteServcio.find({'tailNum': query.tailNum}, (err,partesServcio) =>{
        if(err) return  res.status(500).send({msesage: 'Error en la petición'});
        if(!partesServcio) return  res.status(404).send({msesage: 'No existe partesServcio'});
        if(partesServcio) return  res.status(200).send({
            partesServcio,
        
        });
     
    });
    //correo.enviarCorreo("eric.zubin@gmail.com");
} 
function updateParteServicio(req,res){
    var params=req.body;
   // console.log(params);


   var  parteP= Parte.findOne({"component" :params.component},{},{sort: { 'created_at' : -1 }}).exec();
     
      parteP.then(function (doc2) {

        var  servicioP= Servicio.findOne({"tailNum":params.tailNum},{},{sort: { 'created_at' : -1 }}).exec();

        servicioP.then(function (doc) {

                   // console.log(doc);
            if(doc){
                var parte=new ParteServcio();
                    
                parte.idRFID =params.idRFID;
                parte.serviciod =doc._id;
                parte.parted =doc2._id;
                parte.component =doc2.component;
                parte.tailNum =params.tailNum;
            //  parte.ns =params.ns;




                parte.save((err,parteStored)=>{
                   

                    if(err) return  res.status(500).send({msesage: 'Error al asociacion la parte'});
                    if(parteStored){

                        return res.status(200).send({parteStored}); 
                    }else{
                        return res.status(404).send({msesage: 'No se ah registrado la parte'});
                    }
                });
            }else{
                return res.status(404).send({msesage: 'No se ah registrado la parte'});

            }
          

        });  
       
       

      });

    
   

    
    
        
    
      
     
  
    //correo.enviarCorreo("eric.zubin@gmail.com");
} 

module.exports =
{
    prueba,
    saveparte,
    getPartes,
    updateParteServicio,
    updateParte,
    getPartesServicio,

}