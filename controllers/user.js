'use strict'

var User=require('../models/user');

var bcrypt= require('bcrypt-nodejs');
var  jwt=require('../services/jwt');
var fs=require('fs');
var path=require('path');
//var correo=require('../services/correo');

function home (req,res){
    res.status(200).send({
        msesage: 'Hola Mundo'
    });
    //correo.enviarCorreo("eric.zubin@gmail.com");
} 

function pruebas (req,res){
    res.status(200).send({
        msesage: 'Accion de pruebas'
    });
} 
function recoverpassword (req,res){
    var params=req.body;
    var email =params.email;

   

    User.findOne({email: email}, (err,user)=>{
        if(err) return  res.status(500).send({msesage: 'Error en la petición'});
        if(!user) return  res.status(404).send({msesage: 'No encontro'});

        if(user) {
            //correo.enviarCorreo(user.email,hash);

        }
    }
  );
    res.status(200).send({
        msesage: 'Accion de pruebas'
    });
} 
//Registro de usuarios
function saveUser (req,res){

    var params=req.body;
    var user=new User();

    if(params.name && params.surname && params.nick && params.email && params.password)
    {
        user.name =params.name;
        user.surname =params.surname;
        user.nick =params.nick;
        user.email =params.email;
        user.password =params.password;
        
        user.image =null;
        user.role ='ROLE_USER';

        //controllar usuarios duplicados
        User.find({ $or: [
               {emai: user.email.toLowerCase()},
               {nick: user.nick.toLowerCase()}
            ]}).exec((err,users) =>{
                    if(err) return  res.status(500).send({msesage: 'Error en la petición de usuarios'});
                    if(users && users.length >= 1){
                        return  res.status(200).send({msesage: 'El usuario que intentar registrar ya existe'});
                    }else{
                            //Cifrar Datos de contraseña
                            bcrypt.hash(params.password,null,null,(err,hash)=>{
                                user.password=hash;
                                user.save((err,userStored)=>{
                                    if(err) return  res.status(500).send({msesage: 'Error al guardar el usuario'});

                                    if(userStored){
                                        res.status(200).send({user: userStored}); 
                                    }else{
                                        res.status(404).send({msesage: 'No se ah registrado el usuario'});
                                    }
                                });
                            });
                    }

                });
       
    }else{
        res.status(200).send({
            msesage: 'Envia todos los datos necesarios'
        });
    }
  
} 
//Login
function login (req,res){
    var params=req.body;
    var email=params.email;
    var password =params.password;

    User.findOne({email: email}, (err,user)=>{
        if(err) return  res.status(500).send({msesage: 'Error en la petición'});
        if(user) {
            bcrypt.compare(password, user.password,(err,check)=>{

                if(check){
                    if(params.getToken){
                        //Devolver token
                        res.status(200).send({
                            token: jwt.createToken(user)
                        });

                    }else
                    {
                        user.password=undefined;
                        res.status(200).send({user});
                    }
             

                }else{
                    res.status(404).send({msesage: 'El usuario no se ah podido indentificar'});

                }

            });
        }else{
            res.status(404).send({msesage: 'El usuario no se ah podido indentificar!!'});

        }

    });

}
function getCounters (req,res){
    var userId=req.user.sub;
    if(req.params.id){
        var userId=req.params.id;
    }

    getCountFollow(userId).then((value)=>
    {
        res.status(200).send(value);
    });
} 
async function getCountFollow(user_id){

    var following = await Follow.count({"user":user_id});
 
    var followed = await Follow.count({"followed":user_id});
 
    var publications = await Publication.count({"user":user_id});

     return{
         following:following,
         followed:followed,
         publications:publications,
     }
 }

function getUser (req,res){
    var userId=req.params.id;

    User.findById(userId, (err,user) =>{
        if(err) return  res.status(500).send({msesage: 'Error en la petición'});
        if(!user) return  res.status(404).send({msesage: 'El usuario no existe'});
      
        followThisUser (req.user.sub,userId).then((value)=>{
            return  res.status(200).send({
                user,
                following: value.following,
                followed: value.followed,

            });

        });
    }
    );
  
} 
async function followThisUser(identity_user_id,user_id){

   var following = await Follow.findOne({"user":identity_user_id,"followed":user_id});

    var followed = await Follow.findOne({"user":user_id,"followed":identity_user_id});


    return{
        following:following,
        followed:followed,
    }
}
//Devolverun listadoe usuario parginados
function getUsers (req,res){
    var identity_user_id=req.user.sub;
    var page=1;
    if(req.params.page)
    {
    page=req.params.page;
    }
    var itemsPerPage =5;
    User.find().sort('_id').paginate(page,itemsPerPage,(err,users,total)=>{
        if(err) return  res.status(500).send({msesage: 'Error en la petición'});
        if(!users) return  res.status(404).send({msesage: 'No hay usuarios disponibles'});
        followUsersIds(identity_user_id).then((value)=>{
            return  res.status(200).send({
                users,
                users_following:value.following,
                users_follow_me:value.followed,
                total,
                pages: Math.ceil(total/itemsPerPage)
            });
        });
        

    });

  
}

async function followUsersIds (user_id){
    var following =  await Follow.find({"user":user_id}).select({'_id':0,'__v':0,'user':0});
    var followed =  await Follow.find({"followed":user_id}).select({'_id':0,'__v':0,'user':0});


    var following_clean=[];
    var followed_clean=[];

    await following.forEach((follow)=>{
        following_clean.push(follow.followed);
    });
  
    await followed.forEach((follow)=>{
        followed_clean.push(follow.followed);
    });

    return{
        following:following_clean,
        followed:following_clean,
    }
}
//Edicion de un usuario
function updateUser (req,res){
    var userId=req.params.id;
    var update=req.body;

    delete update.password;

    if(userId != req.user.sub){
        return  res.status(500).send({msesage: 'No tienes permiso para actualizar los datos del usuarios'});
    }
   

 //controllar usuarios duplicados
     User.find({ $or: [
        {emai: update.email.toLowerCase()},
        {nick: update.nick.toLowerCase()}
     ]}).exec((err,users) =>{
             if(err) return  res.status(500).send({msesage: 'Error en la petición de usuarios'});
             var user_isset=false;
            users.forEach((user)=>{
                if(user && user._id != userId) user_isset=true;
            });
             if(user_isset){
                 return  res.status(404).send({msesage: 'El usuario que intentar registrar ya existe'});
             }else{
                     //Cifrar Datos de contraseña
                     User.findByIdAndUpdate(userId,update,{new:true},(err,userUpdate)=>{
                        if(err) return  res.status(500).send({msesage: 'Error en la petición'});
                
                        if(!userUpdate) return  res.status(404).send({msesage: 'No se ah podido actualizar el usuario'});
                
                        return  res.status(200).send({user:userUpdate});
                
                    })
             }

         });

    

  
  
} 
//subir archivos para imagen para el avatar del usuarios
function uploadImage (req,res){
    var userId=req.params.id;


    if(userId != req.user.sub){
        return removeFilesOfUploads(res,file_path,'No tienes permiso para actualizar los datos del usuarios');

    }
   
   if(req.files){
       var file_path=req.files.image.path;
       var file_split=file_path.split('\/');
        var file_name=file_split[2];

        var ext_split=file_path.split('\.');
        var file_ex=ext_split[1];;

        if(file_ex == `png` || file_ex == `jpg` || file_ex == `jpeg` || file_ex == `gif`){
            User.findByIdAndUpdate(userId,{image:file_name},{new:true},(err,userUpdate)=>{
                if(err) return  res.status(500).send({msesage: 'Error en la petición'});

                if(!userUpdate) return  res.status(404).send({msesage: 'No se ah podido actualizar el usuario'});
        
                return  res.status(200).send({user:userUpdate});
            });

        }else{
            return removeFilesOfUploads(res,file_path,'La extension no valida');
        }

   }else{
    return  res.status(200).send({msesage: 'No se han subidos imagenes'});

   }
  
} 

function removeFilesOfUploads(res,file_path,menssage){
    return res.status(200).send({msesage: menssage});

}
function getImageFile (req,res){
    var image_file=req.params.imageFile;
    var path_file='uploads/user/'+image_file;
console.log(image_file);
    fs.exists(path_file,(exits)=>{
        if(exits){
            res.sendFile(path.resolve(path_file));
        }else{
           return  res.status(200).send({msesage: 'No existe la imagen'});

        }
    });

} 


module.exports =
{
    home,
    pruebas,
    saveUser,
    login,
    getUser,
    getUsers,
    updateUser,
    uploadImage,
    getImageFile,
    getCounters
}