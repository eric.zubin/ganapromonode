'use strict'

var RFID=require('../models/rfid');
var RFIDCopia=require('../models/rfidCopia');

var Parte=require('../models/parte');
var Alarma=require('../models/alarma');

var ParteServicio=require('../models/parteservicio');

var bcrypt= require('bcrypt-nodejs');
var  jwt=require('../services/jwt');
var mongooePaginate=require('mongoose-pagination');
var ObjectId = require('mongoose').Types.ObjectId; 
var moment=require('moment');

function home (req,res){
    res.status(200).send({
        msesage: 'Hola Mundo'
    });
  //  correo.enviarCorreo("eric.zubin@gmail.com");
} 

function pruebas (req,res){
    res.status(200).send({
        msesage: 'Accion de pruebas rfid'
    });
} 

//Registro de usuarios
function rfidsave (req,res){

        var params=req.body;

        var str = params.idRFID;
        
        if(str.length > 24){
            var str2=str.substr(8, 24);

            params.idRFID = str2;
        }
      
        
            var  parteP= ParteServicio.findOne({"idRFID":params.idRFID}).sort({created_at: -1}).exec();
            parteP.then(function (doc) {
                try { 
                var  parteP2= RFID.findOne({idRFID:params.idRFID , aparatoIdentificador: params.aparatoIdentificador,parte: doc.parted,servicio: doc.serviciod }).sort({FechaFist: -1}).exec();
                parteP2.then(function(doc2){

              
                    if(doc2 === null || doc2.length === 0){


                        var rfid=new RFID();
                    
                       // console.log("if 1");

                        rfid.componente =doc.component;
                        rfid.aparato =params.aparato;
                        rfid.aparatoIdentificador =params.aparatoIdentificador;
                        rfid.tailNum =doc.tailNum;
                        rfid.idRFID =params.idRFID;
                        rfid.parte =doc.parted;
                        rfid.servicio =doc.serviciod;
                        rfid.tecnico =params.tecnico;
                        if(params.aparatoIdentificador == 11){
                            //Ver si ah pasado por inspeccion 10 cuando pase por el 11

                            var  parteP= RFID.findOne( {idRFID:params.idRFID , aparatoIdentificador: 10 ,parte: doc.parted,servicio: doc.serviciod } ).exec();
                            parteP.then(function(parteFuncionando,err){
            
                                if(parteFuncionando === null || parteFuncionando.length === 0){
                                    var alarma=new Alarma();
                                    alarma.tail=doc.tailNum;
                                    alarma.idRFID =params.idRFID;
                                    alarma.parte =doc.parted;
                                    alarma.servicio =doc.serviciod;
                                    alarma.deteccion = "Antena 2";
                                    alarma.componente= doc.component;
                                    alarma.tipo = 0;
                                    alarma.save((err,alarmaStored)=>{ 
                                        if(err) console.log( 'Error al guardar la alarma');
                                        if(alarmaStored){
                                            console.log( 'Se guardo correctamente la alarma');
                                     
                                        }else{
                                            console.log( 'No se ah registrado correctamente la alarma');

                                        }
                                    });
                                }
                                });
                        }
                       
                    
                       //controllar usuarios duplicados
                        rfid.save((err,rfidStored)=>{
                            if(err) return  res.status(500).send({msesage: 'Error al guardar el rfid'});
                            if(rfidStored){
                                res.status(200).send({rfid: rfidStored}); 
                            }else{
                                res.status(404).send({msesage: 'No se ah registrado el rfid'});
                            }
                        });
                        
                    }else{
                        var rfid=new RFID();
                        
                        if(params.FechaFist == "SI"){
                         

                        }else{
                            rfid.FechaFist = doc2.FechaFist;
                            if(params.aparatoIdentificador == 1  && params.aparato  !=  "Receiving"){
                                //Ver si ah pasado por inspeccion 10 cuando pase por el 11
                                var Fecha1 =  doc2.created_at
                                var fechaEnMiliseg = Date.now();
                                var minutes= moment(fechaEnMiliseg).diff(moment(Fecha1), 'minutes');
                                


                                
                                var  alarmabuscar= Alarma.findOne( {tail:doc.tailNum , idRFID: params.idRFID ,parte: doc.parted,servicio: doc.serviciod ,componente: doc.component,tipo: 0 ,deteccion : "Antena 1"} ).exec();
                                alarmabuscar.then(function(alarmaBuscarResultado,err){
                                    
                                    if( minutes > 10  &&   alarmaBuscarResultado === null     || alarmaBuscarResultado.length === 0 ) {
                                            var  partePRevi= RFID.findOne( {idRFID:params.idRFID , aparatoIdentificador: 10 ,parte: doc.parted,servicio: doc.serviciod } ).exec();
                                            partePRevi.then(function(parteFuncionando,err){
                                            if(parteFuncionando === null || parteFuncionando.length === 0){
                                                var alarma=new Alarma();
                                                alarma.tail=doc.tailNum;
                                                alarma.idRFID =params.idRFID;
                                                alarma.parte =doc.parted;
                                                alarma.servicio =doc.serviciod;
                                                alarma.componente= doc.component;
                                                alarma.tipo = 0;
                                                alarma.deteccion = "Antena 1";
                                                alarma.save((err,alarmaStored)=>{ 
                                                    if(err) console.log( 'Error al guardar la alarma');
                                                    if(alarmaStored){
                                                        console.log( 'Se guardo correctamente la alarma');
                                                 
                                                    }else{
                                                        console.log( 'No se ah registrado correctamente la alarma');
            
                                                    }
                                                });

                                            }


                                        });
                                      
                                    }
                                }
                                );    
                                  
                                   
                            }
                           
                            if(params.aparatoIdentificador == 9){
                                    //Poner alarma de que faltan 20 minutos
                                    var alarma=new Alarma();
                                    alarma.tail=doc.tailNum;
                                    alarma.idRFID =params.idRFID;
                                    alarma.parte =doc.parted;
                                    alarma.servicio =doc.serviciod;
                                    alarma.deteccion = "Empezar el pondometro";
                                    alarma.componente= doc.component;
                                    alarma.tipo = 1;
                                    alarma.estadoLetrero = "DEPLAY";

                                    
                                    alarma.save((err,alarmaStored)=>{
                                        if(err) console.log( 'Error al guardar la alarma 2');
                                        if(alarmaStored){
                                            console.log( 'Se guardo correctamente la alarma 2');
                                     
                                        }else{
                                            console.log( 'No se ah registrado correctamente la alarma 2');

                                        }
                                    });

                                 
                            }else if(params.aparatoIdentificador == 10){

                            

                                 
                                  Alarma.findOne({
                                    tail: doc.tailNum ,idRFID: params.idRFID ,parte: doc.parted, servicio: doc.serviciod
                                  })
                                  .then((alarma) => {
                                    alarma.estadoLetrero = "APPROVED";
                                    alarma.tiempoInspeccion = Date.now() ;

                                    alarma.save().then(() => {
                                        console.log(alarma);
                                      });

                                
                                  });           

                            }
                        }
                       // console.log("if 2");
                        rfid.componente =doc.component;
                        rfid.tailNum =doc.tailNum;
                        rfid.aparato =params.aparato;
                        rfid.aparatoIdentificador =params.aparatoIdentificador;
                        rfid.idRFID =params.idRFID;
                        rfid.parte =doc.parted;
                        rfid.servicio =doc.serviciod;
                        rfid.tecnico =params.tecnico;

                       //controllar usuarios duplicados
                        rfid.save((err,rfidStored)=>{
                            if(err) return  res.status(500).send({msesage: 'Error al guardar el rfid'});
                
                            if(rfidStored){
                                res.status(200).send({rfid: rfidStored}); 
                            }else{
                                res.status(404).send({msesage: 'No se ah registrado el rfid'});
                            }
                        });
                    }

                  
                });
                    }catch(error) {
                        res.status(404).send({msesage: 'No se ah registrado el rfid'});

                    }
            });


           
        
} 



function getRFID (req,res){
    var userId=req.params.id;
    const query = req.query;

    var aray = new Array();

      RFID.aggregate([
          //{ "$match": { "tailNum": query.taill } },
            {   $group: { "_id": {
                 "idRFID": "$idRFID",     
                "aparatoIdentificador": "$aparatoIdentificador","tailNum": "$tailNum","FechaFist": "$FechaFist"},  
                maxBalance: { $max: '$created_at' } , "id": { $max: "$_id"}}},   
                { $project: { _id: 1, maxBalance: 1 , id: 1, component: 1}}]).
                then(function (resultado) {
                        for (var k in resultado) {
                            aray.push(resultado[k].id);
                        }
                        //console.log(query.componenete);
                        /*if(query.componenete != ""){
                            var quer= {$and: [
                                { '_id': aray },
                                { 'componente': query.componenete },
                              ]}
                            }else{*/
                                var quer= 
                                    { '_id': aray }
                                  
                            //} 
                        var  parteP= RFID.find( quer  ).populate('parte').sort({ 'idRFID': -1,'created_at': 1  }).exec();
                        parteP.then(function(rfid,err){
                            if(err) return  res.status(500).send({msesage: 'Error al obtener el rfid'});
                                    
                            if(rfid){

                                res.status(200).send({rfids: rfid}); 
                                
                            }else{
                                res.status(404).send({msesage: 'No se ah registrado el rfid'});
                            }
                            });
                    
                });

} 

function findRfid (req,res){
    var params=req.body;
    var rfid =params.rfid;

   
    ParteServicio.findOne({idRFID: rfid}, {}, { sort: { 'created_at' : -1 } }, (err,parteStored)=>{
        if(err) return  res.status(500).send({msesage: 'Error en la petición'});
        if(!parteStored) return  res.status(404).send({msesage: 'No encontro'});

        if(parteStored) {
            //correo.enviarCorreo(user.email,hash);
            res.status(200).send({parte: parteStored}); 

        }
    }
  );
 
} 


function findLastRfid (req,res){

    var params=req.body;
    
        var  parteP= ParteServicio.findOne({"idRFID":params.idRFID}).sort({created_at: -1}).exec();
        parteP.then(function (doc) {
            try { 
            var  parteP2= RFID.find({idRFID:params.idRFID , aparatoIdentificador: params.aparatoIdentificador,parte: doc.parted,servicio: doc.serviciod }).exec();
            parteP2.then(function(doc2){
          
                if(doc2 === null || doc2.length === 0){

                  //Aqui ya no esta guardado
                  res.status(200).send({status: 0});   
                }else{
                  //Aqui ya esta guardado
                  if(doc2.length === 1){
                    res.status(200).send({status: 1}); 

                  }else{
                    res.status(200).send({status: 2}); 
                  }

                }

            });
                }catch(error) {
                    res.status(404).send({msesage: 'No se ah registrado el rfid'});

                }
        });
} 


function getAlarmas (req,res){
    Alarma.find({}, (err,alarmas) =>{
        if(err) return  res.status(500).send({msesage: 'Error en la petición'});
        if(!alarmas) return  res.status(404).send({msesage: 'No existe alarmas'});
        if(alarmas) return  res.status(200).send({
            alarmas,     
        });
     
    });
} 

function getAlarmasTiempo (req,res){
    Alarma.find({}, (err,alarmas) =>{
        if(err) return  res.status(500).send({msesage: 'Error en la petición'});
        if(!alarmas) return  res.status(404).send({msesage: 'No existe alarmas'});
        if(alarmas) return  res.status(200).send({
            alarmas,     
        });
     
    });
} 


module.exports =
{
    home,
    pruebas,
    rfidsave,
    getRFID,
    findRfid,
    findLastRfid,
    getAlarmas,
}