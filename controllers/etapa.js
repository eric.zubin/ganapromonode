'use strict'

var Etapa=require('../models/etapa');

//var fs=require('fs');
//var path=require('path');
var mongooePaginate=require('mongoose-pagination');
//Prueba del controlador Servicio
function pruebaEtapa (req,res){
    res.status(200).send({
        msesage: 'Hola Mundo desde etapa'
    });
    //correo.enviarCorreo("eric.zubin@gmail.com");
} 
//Registro de etapa
function etapaSave (req,res){

        var params=req.body;

        var etapa=new Etapa();
        etapa.nombre =params.nombre;

        etapa.save((err,etapaStored)=>{
            if(err) return  res.status(500).send({msesage: 'Error al guardar el servicio'});

            if(etapaStored){
                res.status(200).send({etapa: etapaStored}); 
            }else{
                res.status(404).send({msesage: 'No se ah registrado el servicio'});
            }
        });
    
} 

//Funcion del metodo borrar Etapa
function deleteEtapa (req,res){
  

    Etapa.remove({ _id: req.body.id }, function(err) {
        if (!err) {
            res.status(200).send({
                msesage: 'Delete Etapa'
            });
        }
        else {
            res.status(500).send({
                msesage: 'Delete Etapa'
            });
        }
    });
    //correo.enviarCorreo("eric.zubin@gmail.com");
} 


//Funcion del metodo actualizar etapa
function updateEtapa (req,res){
    res.status(200).send({
        msesage: 'Update Servicio'
    });
    //correo.enviarCorreo("eric.zubin@gmail.com");
} 

//Funcion del metodo borrar etapa
function getEtapa (req,res){
    res.status(200).send({
        msesage: 'Get Servicio'
    });
    //correo.enviarCorreo("eric.zubin@gmail.com");
} 

//Funcion del metodo obtener etapas
function getEtapas (req,res){
    Etapa.find({}, (err,etapas) =>{
        if(err) return  res.status(500).send({msesage: 'Error en la petición'});
        if(!etapas) return  res.status(404).send({msesage: 'No existe etapas'});
        if(etapas) return  res.status(200).send({
            etapas,     
        });
     
    });
    //correo.enviarCorreo("eric.zubin@gmail.com");
} 
module.exports =
{
    pruebaEtapa,
    etapaSave,
    getEtapas,

}