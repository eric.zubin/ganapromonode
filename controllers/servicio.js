'use strict'

var Servicio=require('../models/servicio');

//var fs=require('fs');
//var path=require('path');
var mongooePaginate=require('mongoose-pagination');
//Prueba del controlador Servicio
function prueba (req,res){
    res.status(200).send({
        msesage: 'Hola Mundo desde avion'
    });
    //correo.enviarCorreo("eric.zubin@gmail.com");
} 
//Registro de usuarios
function servicioSave (req,res){

        var params=req.body;

        var servicio=new Servicio();
        servicio.tailNum =params.tailNum;
        servicio.type =params.type;
        servicio.date1 =params.date1;
        servicio.date2 =params.date2;
        servicio.lineService =params.lineService;
        
        servicio.airline =params.airline;

        

     

        //controllar usuarios duplicados
        servicio.save((err,servicioStored)=>{
            if(err) return  res.status(500).send({msesage: 'Error al guardar el servicio'});

            if(servicioStored){
                res.status(200).send({servicio: servicioStored}); 
            }else{
                res.status(404).send({msesage: 'No se ah registrado el servicio'});
            }
        });
    
} 

//Funcion del metodo borrar servicio
function deleteServicio (req,res){
  

    Servicio.remove({ _id: req.body.id }, function(err) {
        if (!err) {
            res.status(200).send({
                msesage: 'Delete Servicio'
            });
        }
        else {
            res.status(500).send({
                msesage: 'Delete Servicio'
            });
        }
    });
    //correo.enviarCorreo("eric.zubin@gmail.com");
} 


//Funcion del metodo borrar servicio
function updateServicio (req,res){
    res.status(200).send({
        msesage: 'Update Servicio'
    });
    //correo.enviarCorreo("eric.zubin@gmail.com");
} 

//Funcion del metodo borrar servicio
function getServicio (req,res){
    res.status(200).send({
        msesage: 'Get Servicio'
    });
    //correo.enviarCorreo("eric.zubin@gmail.com");
} 

//Funcion del metodo borrar servicio
function getServicios (req,res){
    Servicio.find({}, (err,servicios) =>{
        if(err) return  res.status(500).send({msesage: 'Error en la petición'});
        if(!servicios) return  res.status(404).send({msesage: 'No existe servicios'});
        if(servicios) return  res.status(200).send({
            servicios,     
        });
     
    });
    //correo.enviarCorreo("eric.zubin@gmail.com");
} 
module.exports =
{
    prueba,
    servicioSave,
    getServicios,

}