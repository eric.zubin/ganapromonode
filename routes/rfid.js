'use strict'
var express= require ('express');

var RFIDController= require ('../controllers/rfid');

var api =express.Router();
var md_auth= require('../middelewares/authenticated');


/**
 * @api {get} /pruebarfid Prueba del controlador RFID
 * @apiVersion 1.0.0
 * @apiName pruebas
 * @apiGroup RFID
 *
 *
 * @apiSuccess {String} Accion de pruebas rfid.
 */
api.get('/pruebarfid', RFIDController.pruebas);
/**
 * @api {post} /rfidsave Registrar un nuevo rfid
 * @apiVersion 1.0.0
 * @apiName GetUser
 * @apiGroup RFID
 * @apiParam {String} idRFID idRFID.
 * @apiParam {String} aparato aparato.
 * @apiParam {String} aparatoIdentificador aparatoIdentificador.
 *
 * @apiSuccess {String} idRFID idRFID.
 * @apiSuccess {String} aparato aparato.
 * @apiSuccess {String} aparatoIdentificador aparatoIdentificador.
 */
api.post('/rfidsave', RFIDController.rfidsave);

/**
 * @api {get} /getRFID Obtener los RFIDs
 * @apiVersion 1.0.0
 * @apiName getRFID
 * @apiGroup RFID
 *
 *
 * @apiSuccess {JSON} rfids Se obtiene una lista de los rfids que han ingresado al sistema agrupados por aparatoIdentificador
 */
api.get('/getRFID', RFIDController.getRFID);


/**
 * @api {post} /findRfid Obtener la parte
 * @apiVersion 1.0.0
 * @apiName findRfid
 * @apiGroup RFID
 * @apiParam {String} rfid El id a buscar.
 *
 *
 * @apiSuccess {JSON} parte Se obtiene que parte pertenece a ese ifrfid
 */
api.post('/findRfid', RFIDController.findRfid);


/**
 * @api {post} /findLastRfid Obtener el ultimo RFID
 * @apiVersion 1.0.0
 * @apiName findLastRfid
 * @apiGroup RFID
 * @apiParam {String} idRFID idRFID.
 * @apiParam {String} aparatoIdentificador aparatoIdentificador.
 *
 *
 * @apiSuccess {JSON} status Se sabe si ya hubo antes
 */
api.post('/findLastRfid', RFIDController.findLastRfid);



//TODO: Ruta getAlarmas por mover a otro contolador
/**
 * @api {get} /getAlarmas Obtener el ultimo RFID
 * @apiVersion 1.0.0
 * @apiName getAlarmas
 * @apiGroup RFID
 *
 *
 * @apiSuccess {JSON} alertas Las alertas
 */
api.get('/getAlarmas', RFIDController.getAlarmas);


module.exports=api;
