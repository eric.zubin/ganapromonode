'use strict'
var express= require ('express');

var ParteController= require ('../controllers/parte');

var api =express.Router();
var md_auth= require('../middelewares/authenticated');

/**
 * @api {get} /prueba_partes Prueba del controlador Parte
 * @apiVersion 1.0.0
 * @apiName prueba
 * @apiGroup Parte
 *
 *
 * @apiSuccess {String} Accion de pruebas parte.
 */



api.get('/prueba_partes', ParteController.prueba);



/**
 * @api {post} /parte_save Registrar ua nueva parte con sus tiempo
 * @apiVersion 1.0.0
 * @apiName SaveParte
 * @apiGroup Parte
 *
 * @apiParam {String} component component
 *
 *
 * @apiSuccess {String} component component
 * 
 */
api.post('/parte_save', ParteController.saveparte);

/**
 * @api {get}  /get_partes Regresa  las partes ingresadas
 * @apiVersion 1.0.0
 * @apiName get_partes
 * @apiGroup Parte
 *
 *
 * @apiSuccess {String} partes Devolver partes ingresadas en el sistema.
 */
api.get('/get_partes', ParteController.getPartes);

/**
 * @api {get}  /parte_servicio/:tailNum Regresa  las partes ingresadas
 * @apiVersion 1.0.0
 * @apiParam {String} tailNum El taill number del avion.
 * @apiName get_partes
 * @apiGroup Parte
 *
 *
 * @apiSuccess {String} Devolver partes.
 */
api.get('/parte_servicio/', ParteController.getPartesServicio);


/**
 * @api {post}  /update_partes Ingresa las partes de nombre de las partes, el numService y el rfid
 * @apiVersion 1.0.0
 * @apiName update_partes
 * @apiGroup Parte
 * @apiParam {String} rfid El Rfid con el que se va a ligar.
 * @apiParam {parte} parte El nombre de la parte.
 * @apiParam {String} numService numService.
 *
 * @apiSuccess {String} rfid Devolver el rfid.
 * @apiSuccess {String} parte DEvolver el id de la parte.
 * @apiSuccess {String} numService Devolver el id del Servicio.
 */
api.post('/update_partes_servicio', ParteController.updateParteServicio);



api.put('/updateParte', ParteController.updateParte);

module.exports=api;
