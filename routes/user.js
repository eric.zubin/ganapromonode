'use strict'

var express= require ('express');
var UserController= require ('../controllers/user');
var api =express.Router();
var md_auth= require('../middelewares/authenticated');
var multipart= require('connect-multiparty');

var md_upload=multipart({uploadDir: 'uploads/user'});

/**
 * @api {get} /user/:id Registrar un nuevo usuario
 * @apiVersion 1.0.0
 * @apiName GetUser
 * @apiGroup User
 *
 * @apiParam {Number} id Users unique ID.
 *
 * @apiSuccess {String} firstname Firstname of the User.
 * @apiSuccess {String} lastname  Lastname of the User.
 */
api.post('/registrer', UserController.saveUser);
/**
 * @api {get} /user/:id Registrar un nuevo usuario
 * @apiVersion 1.0.0
 * @apiName GetUser
 * @apiGroup User
 *
 * @apiParam {Number} id Users unique ID.
 *
 * @apiSuccess {String} firstname Firstname of the User.
 * @apiSuccess {String} lastname  Lastname of the User.
 */
api.post('/login', UserController.login);
/**
 * @api {get} /user/:id Registrar un nuevo usuario
 * @apiVersion 1.0.0
 * @apiName GetUser
 * @apiGroup User
 *
 * @apiParam {Number} id Users unique ID.
 *
 * @apiSuccess {String} firstname Firstname of the User.
 * @apiSuccess {String} lastname  Lastname of the User.
 */
api.get('/user/:id', md_auth.ensureAuth,UserController.getUser);
/**
 * @api {get} /user/:id Registrar un nuevo usuario
 * @apiVersion 1.0.0
 * @apiName GetUser
 * @apiGroup User
 *
 * @apiParam {Number} id Users unique ID.
 *
 * @apiSuccess {String} firstname Firstname of the User.
 * @apiSuccess {String} lastname  Lastname of the User.
 */
api.get('/users/:page?', md_auth.ensureAuth,UserController.getUsers);
/**
 * @api {get} /user/:id Registrar un nuevo usuario
 * @apiVersion 1.0.0
 * @apiName GetUser
 * @apiGroup User
 *
 * @apiParam {Number} id Users unique ID.
 *
 * @apiSuccess {String} firstname Firstname of the User.
 * @apiSuccess {String} lastname  Lastname of the User.
 */
api.put('/update-user/:id', md_auth.ensureAuth,UserController.updateUser);
/**
 * @api {get} /user/:id Registrar un nuevo usuario
 * @apiVersion 1.0.0
 * @apiName GetUser
 * @apiGroup User
 *
 * @apiParam {Number} id Users unique ID.
 *
 * @apiSuccess {String} firstname Firstname of the User.
 * @apiSuccess {String} lastname  Lastname of the User.
 */
api.post('/upload-image-user/:id', [md_auth.ensureAuth,md_upload],UserController.uploadImage);
/**
 * @api {get} /user/:id Registrar un nuevo usuario
 * @apiVersion 1.0.0
 * @apiName GetUser
 * @apiGroup User
 *
 * @apiParam {Number} id Users unique ID.
 *
 * @apiSuccess {String} firstname Firstname of the User.
 * @apiSuccess {String} lastname  Lastname of the User.
 */
api.get('/get-image-user/:imageFile', UserController.getImageFile);





module.exports=api;

