'use strict'
var express= require ('express');

var ServicioController= require ('../controllers/servicio');

var api =express.Router();
var md_auth= require('../middelewares/authenticated');


/**
 * @api {get} /pruebaServicio Prueba del controlador servicio
 * @apiVersion 1.0.0
 * @apiName pruebaServicio
 * @apiGroup Servicio
 *
 *
 * @apiSuccess {String} Hola Mundo desde avion.
 */
api.get('/pruebaServicio', ServicioController.prueba);

/**
 * @api {servicioSave} Guardar el servicio
 * @apiVersion 1.0.0
 * @apiName servicioSave
 * @apiGroup Servicio
 *
 * @apiParam {String} model model.
 * @apiParam {String} tailNum tailNum.
 * @apiParam {String} type type.
 * @apiParam {String} airline airline.
 * @apiParam {String} date1 date1.
 * @apiParam {String} date2 date2.
 * @apiParam {String} lineService lineService.
 *
 * @apiSuccess {String} model model.
 * @apiSuccess {String} tailNum tailNum.
 * @apiSuccess {String} type type.
 * @apiSuccess {String} airline airline.
 * @apiSuccess {String} date1 date1.
 * @apiSuccess {String} date2 date2.
 * @apiSuccess {String} lineService lineService.
 * @apiSuccess {String} created_at created_at.
 * 
 */
api.post('/save_Servicio', ServicioController.servicioSave);



/**
 * @api {get_partes}} Regresar los servicios ingresadas
 * @apiVersion 1.0.0
 * @apiName get_servicios
 * @apiGroup Servicio
 *
 *
 * @apiSuccess {JSON} servicios Devolver los servicos dados de alta.
 */
api.get('/get_servicios', ServicioController.getServicios);

module.exports=api;
