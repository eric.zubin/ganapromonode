'use strict'
var express= require ('express');

var EtapaController= require ('../controllers/etapa');

var api =express.Router();
var md_auth= require('../middelewares/authenticated');


/**
 * @api {get} /pruebaServicio Prueba del controlador etapa
 * @apiVersion 1.0.0
 * @apiName pruebaServicio
 * @apiGroup Etapa
 *
 *
 * @apiSuccess {String} Hola Mundo desde etapa.
 */
api.get('/pruebaEtapa', EtapaController.pruebaEtapa);

/**
 * @api {post} /save_etapa Guardar la etapa
 * @apiVersion 1.0.0
 * @apiName etapaSave
 * @apiGroup Etapa
 *
 * @apiParam {String} nombre nombre.
 *
 * @apiSuccess {String} nombre nombre.
 * 
 */
api.post('/save_etapa', EtapaController.etapaSave);



/**
 * @api {get} /get_etapas Regresar las etapas
 * @apiVersion 1.0.0
 * @apiName get_etapas
 * @apiGroup Etapa
 *
 *
 * @apiSuccess {JSON} servicios Devolver los servicos dados de alta.
 */
api.get('/get_etapas', EtapaController.getEtapas);

module.exports=api;
