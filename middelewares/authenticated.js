'use strict'

var jwt=require('jwt-simple');
var moment=require('moment');
var secret = 'clave_secreta_curso';

exports.ensureAuth=function(req,res,next){
    if(!req.headers.authorization){
        res.status(403).send({msesage: 'La peticion no tiene la cabecera de auntentificación'});
    }
    var token= req.headers.authorization.replace(/\'"/g, "");

    try{
        var payload=jwt.decode(token,secret);
        if(payload.exp <= moment.unix()){
            res.status(401).send({msesage: 'El token ah expirado'});

        }

    }catch(ex){
        res.status(404).send({msesage: 'El token no es valido'});

    }
    req.user= payload;

    next();
};