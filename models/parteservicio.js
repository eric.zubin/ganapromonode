
'use strict'

var mongoose= require('mongoose');
var Schema= mongoose.Schema;

var parteservicioSchema= Schema({
   
  
    component: String, //AIRLINE 
    tailNum: String,  //
    created_at: { type: Date, default: Date.now },
    idRFID: String, //tag
    ns: String, //ns
    timeServe: String, //timeServe
    tecnico: String, //timeServe


    
    user: {type: Schema.ObjectId, ref: 'User'},
    servicio: {type: Schema.ObjectId, ref: 'Servicio'},
    usuarioModifica: {type: Schema.ObjectId, ref: 'User'},
    
    parted: {type: Schema.ObjectId, ref: 'parte'},
    serviciod: {type: Schema.ObjectId, ref: 'servicio'},

    
});

module.exports = mongoose.model('parte_servicio', parteservicioSchema);