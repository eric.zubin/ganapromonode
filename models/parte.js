
'use strict'

var mongoose= require('mongoose');
var Schema= mongoose.Schema;

var parteSchema= Schema({

    component: String, //AIRLINE 
    created_at: { type: Date, default: Date.now },


    user: {type: Schema.ObjectId, ref: 'User'},
    usuarioModifica: {type: Schema.ObjectId, ref: 'User'},

});

module.exports = mongoose.model('parte', parteSchema);