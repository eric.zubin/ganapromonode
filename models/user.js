'use strict'
var mongoose= require('mongoose');
var Schema= mongoose.Schema;

var UserSchema= Schema({
    name: String,
    surname: String,
    nick: String,
    email: String,
    password: String,
    status: String,
    role: String,
    superior: {type: Schema.ObjectId, ref: 'User'},

    usuarioModifica: {type: Schema.ObjectId, ref: 'User'},

});

module.exports = mongoose.model('User', UserSchema);