
'use strict'

var mongoose= require('mongoose');
var Schema= mongoose.Schema;

var etapaSchema= Schema({

        nombre: String, //AIRLINE 
        created_at: { type: Date, default: Date.now },

});

module.exports = mongoose.model('etapa', etapaSchema);