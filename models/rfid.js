'use strict'
var mongoose= require('mongoose');
var Schema= mongoose.Schema;

var RFIDSchema= Schema({
        idRFID: String,
        aparato: String,
        aparatoIdentificador: Number,
      
        created_at: { type: Date, default: Date.now },
        FechaFist: { type: Date, default: Date.now },

        //En donde se supone que lo leyo
         status: String,
         componente: String,

         tecnico: String,
         tailNum: String,  //


         servicio: {type: Schema.ObjectId, ref: 'servicio'},
         parte: {type: Schema.ObjectId, ref: 'parte'},

       // parte: {type: Schema.ObjectId, ref: 'Parte'},
});

module.exports = mongoose.model('RFID', RFIDSchema);