
'use strict'

var mongoose= require('mongoose');
var Schema= mongoose.Schema;

var alarmaSchema= Schema({

        tail: String,  
        idRFID: String,  
        parte: String,  
        servicio: String,  
        estadoLetrero: String,  
        tiempoInspeccion:  { type: Date, default: Date.now },  
        deteccion: String,  
        componente: String,  
        tipo: Number,  
        created_at: { type: Date, default: Date.now },
        
});

module.exports = mongoose.model('alarma', alarmaSchema);