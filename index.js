'use strict'

var mongoose = require('mongoose');
var app=require('./app');
var port = 3801;

mongoose.Promise = global.Promise;

mongoose.connect("mongodb://localhost:27017/solucion_rfid", { useNewUrlParser: true });

app.listen(port, ()=> {
    console.log("Servidor corriendo");

});

