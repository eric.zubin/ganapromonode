'use strict'

var express = require('express');
var bodyParser = require('body-parser');

var app= express();

//Cargar rutas

//middlewares
 app.use(bodyParser.urlencoded({extended:false}));
 app.use(bodyParser.json());

 //Cargar rutas

 var user_routes= require('./routes/user');
 var rfid_routes= require('./routes/rfid');
 var servicio_routes= require('./routes/servicio');
 var partes_routes= require('./routes/parte');
 var etapas_routes= require('./routes/etapa');


 

//cors
// configurar cabeceras http
app.use((req, res, next) => {
    res.header('Access-Control-Allow-Origin', '*');
    res.header('Access-Control-Allow-Headers', 'Authorization, X-API-KEY, Origin, X-Requested-With, Content-Type, Accept, Access-Control-Allow-Request-Method');
    res.header('Access-Control-Allow-Methods', 'GET, POST, OPTIONS, PUT, DELETE');
    res.header('Allow', 'GET, POST, OPTIONS, PUT, DELETE');
 
    next();
});
//rutas
app.use('/api',user_routes);
app.use('/api',rfid_routes);
app.use('/api',servicio_routes);
app.use('/api',etapas_routes);
app.use('/api',partes_routes);




//Es la documentación de la api
app.use('/apidoc', express.static('doc'));


//exportar

module.exports=app;